# Stratum Server w/ client

## How to build 
1. Start the terminal in the root of this repository
2. Run ```go run server/server.go``` to start the server
3. Run ```go run client/client.go``` to start the client

## Comments
I try to implement ```github.com/ethereum/go-ethereum/rpc``` which was very good because it allows me to create 
subscriptions, so I can notify the client.
Although, I couldn't achieve the goal. I found the subscription quite unintuitive, and documentation wasn't very good
neither.
