module github.com/luxor-technical-texs

go 1.16

require (
	github.com/DATA-DOG/go-sqlmock v1.5.0
	github.com/ethereum/go-ethereum v1.10.4
	github.com/lib/pq v1.0.0
	github.com/stretchr/testify v1.7.0
)
