package models

type AuthorizationInput struct {
	Username string
	Password string
}

type Authorization struct {
	WorkerName string
	Password   string
}
