package models

type Subscription struct {
	SubscriptionDifficulty string
	SubscriptionID         string
	ExtraNonce1            string
	ExtraNonce2            int
}

type SubscriptionResponse struct {
	SubscriptionKeys []SubscriptionKeys
	ExtraNonce1      string
	ExtraNonce2      int
}

type SubscriptionKeys struct {
	SubscriptionType, ID string
}
