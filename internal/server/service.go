package server

import (
	"fmt"
	"github.com/luxor-technical-texs/internal/models"
	"github.com/luxor-technical-texs/internal/platform/db"
	"github.com/luxor-technical-texs/internal/platform/randomgenerator"
	"log"
)

const extranonce_size = 4

type Service interface {
	Authorize(input models.AuthorizationInput) (bool, error)
	Subscribe() (models.SubscriptionResponse, error)
	Notify()
}

type service struct {
	storage       db.Service
	randomService randomgenerator.Service
}

func NewService(db db.Service, randomService randomgenerator.Service) service {
	return service{storage: db, randomService: randomService}
}

func (s *service) Authorize(input models.AuthorizationInput) (bool, error) {
	err := s.storage.SaveAuthorization(models.Authorization{
		WorkerName: input.Username,
		Password:   input.Password,
	})
	if err != nil {
		return false, fmt.Errorf("error on saving authorization: %w", err)
	}

	return true, nil
}

func (s *service) Subscribe() (models.SubscriptionResponse, error) {
	subscription := models.Subscription{
		SubscriptionDifficulty: s.randomService.RandStringRunes(32),
		SubscriptionID:         s.randomService.RandStringRunes(32),
		ExtraNonce1:            s.randomService.RandStringRunes(8),
		ExtraNonce2:            extranonce_size,
	}

	err := s.storage.SaveSubscription(subscription)
	if err != nil {
		return models.SubscriptionResponse{}, fmt.Errorf("fail to save: %w", err)
	}

	return models.SubscriptionResponse{
		SubscriptionKeys: []models.SubscriptionKeys{
			{
				SubscriptionType: "mining.set_difficulty",
				ID:               subscription.SubscriptionDifficulty,
			},
			{
				SubscriptionType: "mining.notify",
				ID:               subscription.SubscriptionID,
			},
		},
		ExtraNonce1: subscription.ExtraNonce1,
		ExtraNonce2: subscription.ExtraNonce2,
	}, nil
}

func (s *service) Notify() {
	log.Println("Notify")
}
