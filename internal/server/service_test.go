package server

import (
	"github.com/luxor-technical-texs/internal/models"
	"github.com/luxor-technical-texs/internal/platform/mocks"
	"github.com/stretchr/testify/mock"
	"reflect"
	"testing"
)

func Test_service_Subscribe(t *testing.T) {
	subscriptionID := "b4b6693b72a50c7116db18d6497cac52"
	extraNonce1 := "08000002"
	randomServiceMock := new(mocks.RandomServiceMock)
	randomServiceMock.On("RandStringRunes", 32).Return(subscriptionID)
	randomServiceMock.On("RandStringRunes", 8).Return(extraNonce1)

	storageMock := new(mocks.StorageServiceMock)
	storageMock.On("SaveSubscription", mock.Anything).Return(nil)

	s := &service{
		storage:       storageMock,
		randomService: randomServiceMock,
	}

	tests := []struct {
		name    string
		want    models.SubscriptionResponse
		wantErr bool
	}{
		{
			name: "",
			want: models.SubscriptionResponse{
				SubscriptionKeys: []models.SubscriptionKeys{
					{
						SubscriptionType: "mining.set_difficulty",
						ID:               subscriptionID,
					},
					{
						SubscriptionType: "mining.notify",
						ID:               subscriptionID,
					},
				},
				ExtraNonce1: extraNonce1,
				ExtraNonce2: 4,
			},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := s.Subscribe()
			if (err != nil) != tt.wantErr {
				t.Errorf("Subscribe() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Subscribe() got = %v, want %v", got, tt.want)
			}
		})
	}
}
