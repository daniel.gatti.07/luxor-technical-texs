package db

import (
	"database/sql"
	"fmt"
	"github.com/luxor-technical-texs/internal/models"
)

type Service interface {
	SaveSubscription(subscription models.Subscription) error
	SaveAuthorization(authorization models.Authorization) error
}

type dbService struct {
	db *sql.DB
}

func NewService(db *sql.DB) *dbService {
	return &dbService{db: db}
}

func (s dbService) SaveSubscription(subscription models.Subscription) error {
	sqlStatement := `
	INSERT INTO subscriptions (difficulty_id, notify_id, extranonce1)
	VALUES ($1, $2, $3)`
	_, err := s.db.Exec(sqlStatement, subscription.SubscriptionDifficulty, subscription.SubscriptionID, subscription.ExtraNonce1)
	if err != nil {
		return fmt.Errorf("fail query row: %w", err)
	}
	return nil
}

func (s dbService) SaveAuthorization(authorization models.Authorization) error {
	sqlStatement := `
	INSERT INTO authorizations (worker_name, password)
	VALUES ($1, $2)`
	_, err := s.db.Exec(sqlStatement, authorization.WorkerName, authorization.Password)
	if err != nil {
		return fmt.Errorf("fail query row: %w", err)
	}
	return nil
}
