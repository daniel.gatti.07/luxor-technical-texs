package db

import (
	"github.com/DATA-DOG/go-sqlmock"
	"github.com/luxor-technical-texs/internal/models"
	"github.com/stretchr/testify/assert"
	"testing"
)

func Test_dbService_SaveSubscription(t *testing.T) {
	db, mock, err := sqlmock.New()
	assert.NoError(t, err)

	s := dbService{
		db: db,
	}

	subs := models.Subscription{
		SubscriptionDifficulty: "b4b6693b72a50c7116db18d6497cac52",
		SubscriptionID:         "ae6812eb4cd7735a302a8a9dd95cf71f",
		ExtraNonce1:            "08000002",
	}

	mock.ExpectExec(`INSERT INTO subscriptions`).
		WithArgs(subs.SubscriptionDifficulty, subs.SubscriptionID, subs.ExtraNonce1).
		WillReturnResult(sqlmock.NewResult(0, 1))

	err = s.SaveSubscription(subs)
	assert.NoError(t, err)
}
