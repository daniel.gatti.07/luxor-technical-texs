package randomgenerator

import (
	"errors"
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestRandStringRunesRandomness(t *testing.T) {
	rdmValues := make(map[string]bool)
	rs := NewService()

	for i := 0; i < 1000; i++ {
		value := rs.RandStringRunes(32)
		if _, exists := rdmValues[value]; exists {
			assert.Error(t, errors.New("this isn't very random"))
		}
		rdmValues[value] = true
	}
}
