package mocks

import "github.com/stretchr/testify/mock"

type RandomServiceMock struct {
	mock.Mock
}

func (s *RandomServiceMock) RandStringRunes(n int) string {
	called := s.Called(n)
	return called.Get(0).(string)
}
