package mocks

import (
	"github.com/luxor-technical-texs/internal/models"
	"github.com/stretchr/testify/mock"
)

type StorageServiceMock struct {
	mock.Mock
}

func (s *StorageServiceMock) SaveAuthorization(authorization models.Authorization) error {
	called := s.Called(s)
	return called.Error(0)
}

func (s *StorageServiceMock) SaveSubscription(_ models.Subscription) error {
	called := s.Called(s)
	return called.Error(0)
}
