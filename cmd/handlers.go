package cmd

import (
	"context"
	"github.com/ethereum/go-ethereum/rpc"
	"github.com/luxor-technical-texs/internal/models"
	"github.com/luxor-technical-texs/internal/server"
	"log"
)

type mining struct {
	ServerService server.Service
}

func NewServerHandler(serverService server.Service) mining {
	return mining{ServerService: serverService}
}

func (s mining) Subscrib() (subscribeRes models.SubscriptionResponse, err error) {
	subscribeRes, err = s.ServerService.Subscribe()
	return
}

func (s mining) Authorize(authReq models.AuthorizationInput) (result bool, err error) {
	result, err = s.ServerService.Authorize(authReq)
	return
}

func (s mining) Notify(ctx context.Context) (rpc.Subscription, error) {
	log.Println("Notify")
	return rpc.Subscription{ID: rpc.NewID()}, nil
}
