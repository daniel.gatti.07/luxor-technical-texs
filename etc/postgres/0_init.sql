CREATE TABLE public.subscriptions (
    id SERIAL PRIMARY KEY,
    difficulty_id CHAR(32) UNIQUE NOT NULL,
    notify_id CHAR(32) UNIQUE NOT NULL,
    extranonce1 CHAR(8) UNIQUE NOT NULL,
    created_at timestamp without time zone DEFAULT now() NOT NULL
);

CREATE TABLE public.authorizations (
  id SERIAL PRIMARY KEY,
  -- subscription_id INT NOT NULL,
  worker_name VARCHAR(256) NOT NULL,
  password VARCHAR(256) NOT NULL,
  created_at timestamp without time zone DEFAULT now() NOT NULL
  -- UNIQUE (subscription_id, worker_name)
  -- CONSTRAINT fk_subscription_id FOREIGN KEY (subscription_id) REFERENCES subscriptions (id)
);

CREATE TABLE public.jobs (
   id SERIAL PRIMARY KEY,
   previous_hash VARCHAR(64),
   coinb1 VARCHAR(128),
   coinb2 VARCHAR(128),
   merkle_branch VARCHAR(256),
   version VARCHAR(8),
   nbits VARCHAR(8),
   ntime VARCHAR(8),
   clean_jobs BIT,
   created_at timestamp without time zone DEFAULT now() NOT NULL
);