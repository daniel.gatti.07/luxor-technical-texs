package main

import (
	"context"
	"fmt"
	"github.com/ethereum/go-ethereum/rpc"
	"github.com/luxor-technical-texs/internal/models"
	"log"
)

func main() {
	// Connect the client.
	client, err := rpc.Dial("ws://127.0.0.1:8080")
	if err != nil {
		log.Fatal(fmt.Errorf("fail not connect to localhost:8080: %w", err))
	}

	ctx := context.Background()

	subsResp := models.SubscriptionResponse{}
	err = client.CallContext(ctx, &subsResp, "mining_subscrib")
	if err != nil {
		log.Fatal(fmt.Errorf("fail subscription: %w", err))
	}

	var auth bool
	err = client.CallContext(ctx, &auth, "mining_authorize", models.AuthorizationInput{
		Username: "user",
		Password: "pass",
	})
	if err != nil {
		fmt.Println("can't get latest block:", err)
		return
	}
}
