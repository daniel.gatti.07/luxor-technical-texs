package main

import (
	"database/sql"
	"fmt"
	"github.com/ethereum/go-ethereum/rpc"
	_ "github.com/lib/pq"
	"github.com/luxor-technical-texs/cmd"
	"github.com/luxor-technical-texs/internal/platform/db"
	"github.com/luxor-technical-texs/internal/platform/randomgenerator"
	"github.com/luxor-technical-texs/internal/server"
	"log"
	"net/http"
	"os"
	"strconv"
)

func main() {
	dbConn := createDBConnection()
	defer dbConn.Close()

	dbService := db.NewService(dbConn)

	serverService := server.NewService(dbService, randomgenerator.NewService())

	serverHandlers := cmd.NewServerHandler(&serverService)

	server := rpc.NewServer()
	err := server.RegisterName("mining", serverHandlers)
	if err != nil {
		log.Fatal(fmt.Errorf("fail registering name: %w", err))
	}

	handler := server.WebsocketHandler([]string{"*"})

	http.HandleFunc("/", handler.ServeHTTP)
	err = http.ListenAndServe("localhost:8080", nil)
	if err != nil {
		log.Fatal(fmt.Errorf("fail listening service: %w", err))
	}
}

func createDBConnection() *sql.DB {
	host := os.Getenv("host")
	if host == "" {
		host = "localhost"
	}

	port, _ := strconv.Atoi(os.Getenv("port"))
	if port == 0 {
		port = 5432
	}

	user := os.Getenv("user")
	if user == "" {
		user = "luxor"
	}

	password := os.Getenv("password")
	if password == "" {
		password = "luxor"
	}

	dbname := os.Getenv("dbname")
	if dbname == "" {
		dbname = "luxor"
	}

	connInfo := fmt.Sprintf("host=%s port=%d user=%s password=%s dbname=%s sslmode=disable",
		host, port, user, password, dbname)

	db, err := sql.Open("postgres", connInfo)
	if err != nil {
		log.Panicln(err)
	}

	err = db.Ping()
	if err != nil {
		log.Panicln(err)
	}
	fmt.Println("connection with db established")
	return db
}
